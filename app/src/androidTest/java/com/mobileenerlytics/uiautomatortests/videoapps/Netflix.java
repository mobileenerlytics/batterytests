package com.mobileenerlytics.uiautomatortests.videoapps;


import android.support.test.uiautomator.UiObject;
import android.support.test.uiautomator.UiObjectNotFoundException;
import android.support.test.uiautomator.UiScrollable;
import android.support.test.uiautomator.UiSelector;

import org.junit.After;
import org.junit.Before;

/**
 * USAGE:
 *      - Test written for Nexus 6 (1440x2560 pixels)
 *      - Assumes EagleTester installed and setup
 *      - Assumes app installed
 *      - Searches for the song given in SEARCH_TEXT
 *      - User name, password of the app and play time of the video can be passed from the command line
 *        or can be given inside the config.properties file.
 *      - Check the Config class for the app specific properties
 */

public class Netflix extends BaseVideo{

    private static final String PACKAGE_NAME = "com.netflix.mediaclient";

    private static final String SEARCH_TEXT = "Definitely maybe";

    @Before
    public void setUpApp() throws Exception {
        super.setUpApp(PACKAGE_NAME,"netflix");
    }

    @Override
    protected void login(String userName, String pwd) throws Exception {
        UiObject none = mDevice.findObject(new UiSelector().text("None of the above"));
        if(none.exists()){
            none.click();
        }
        UiObject signIn = mDevice.findObject(new UiSelector().text("Sign In"));
        if(signIn.exists()){
            signIn.click();
        }
        mDevice.findObject(new UiSelector().resourceId("com.netflix.mediaclient:id/login_email")).setText(userName);
        mDevice.findObject(new UiSelector().resourceId("com.netflix.mediaclient:id/login_password")).setText(pwd);
        mDevice.findObject(new UiSelector().resourceId("com.netflix.mediaclient:id/login_action_btn")).click();
        Thread.sleep(10000);
        mDevice.findObject(new UiSelector().resourceId("com.netflix.mediaclient:id/profile_avatar_title").text("SaiKruthi")).waitForExists(30000);
        mDevice.findObject(new UiSelector().resourceId("com.netflix.mediaclient:id/profile_avatar_title").text("SaiKruthi")).click();
    }

    @Override
    protected void searchVideo() throws UiObjectNotFoundException {
        mDevice.findObject(new UiSelector().resourceId("com.netflix.mediaclient:id/ab_menu_search_item")).click();
        mDevice.findObject(new UiSelector().resourceId("android:id/search_src_text")).setText(SEARCH_TEXT);
    }

    @Override
    protected void playVideo() throws Exception {
        searchVideo();
        mDevice.findObject(new UiSelector().resourceId("com.netflix.mediaclient:id/search_result_img")).click();
        mDevice.findObject(new UiSelector().resourceId("com.netflix.mediaclient:id/video_play_icon")).click();
        super.playVideo();
        mDevice.pressBack();
        mDevice.pressBack();
        mDevice.pressBack();
    }

    @Override
    protected void signout() throws Exception {
        mDevice.findObject(new UiSelector().descriptionContains("Navigate up")).click();
        UiSelector frameLayout = new UiSelector().resourceId("com.netflix.mediaclient:id/sliding_menu_genres_frame");
        UiScrollable scroll = new UiScrollable(frameLayout);
        scroll.setAsVerticalList();
        scroll.scrollToEnd(5);
        mDevice.findObject(new UiSelector().resourceId("com.netflix.mediaclient:id/sliding_menu_row_text").text("Sign Out")).click();
        mDevice.findObject(new UiSelector().resourceId("android:id/button1").text("Sign Out")).click();
    }
}
