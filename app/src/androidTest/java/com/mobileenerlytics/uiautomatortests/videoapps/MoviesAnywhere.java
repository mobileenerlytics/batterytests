package com.mobileenerlytics.uiautomatortests.videoapps;

import android.provider.Contacts;
import android.support.test.runner.AndroidJUnit4;
import android.support.test.uiautomator.UiObjectNotFoundException;
import android.support.test.uiautomator.UiScrollable;
import android.support.test.uiautomator.UiSelector;

import org.junit.After;
import org.junit.Before;
import org.junit.runner.RunWith;

/**
 * USAGE:
 *      - Test written for Nexus 6 (1440x2560 pixels)
 *      - Assumes EagleTester installed and setup
 *      - Assumes app installed
 *      - Searches for the song given in SEARCH_TEXT
 *      - User name, password of the app and play time of the video can be passed from the command line
 *        or can be given inside the config.properties file.
 *      - Check the Config class for the app specific properties
 */

@RunWith(AndroidJUnit4.class)
public class MoviesAnywhere extends BaseVideo {
    private static final int LAUNCH_TIMEOUT = 5000;
    private static final String PACKAGE_NAME = "com.moviesanywhere.goo";

    private static final String Search_Text = "Confessions of a shopaholic";


    @Before
    public void setUpApp() throws Exception {
        super.setUpApp(PACKAGE_NAME, "moviesanywhere");
    }

    @Override
    protected void login(String userName, String pwd) throws UiObjectNotFoundException, InterruptedException {
        mDevice.findObject(new UiSelector().resourceId("com.moviesanywhere.goo:id/button_text").text("Log In")).click();
        Thread.sleep(5000);
        mDevice.findObject(new UiSelector().resourceId("com.moviesanywhere.goo:id/button_text").text("Google")).click();
        Thread.sleep(1000);
        mDevice.findObject(new UiSelector().resourceId("com.google.android.gms:id/account_name").text(userName)).click();
        Thread.sleep(30000);
    }

    @Override
    protected void searchVideo() throws UiObjectNotFoundException, InterruptedException {
        mDevice.findObject(new UiSelector().resourceId("com.moviesanywhere.goo:id/search_menu_item")).click();
        mDevice.findObject(new UiSelector().resourceId("com.moviesanywhere.goo:id/search").text("Search for movies & more")).setText(Search_Text);
        Thread.sleep(1000);
        mDevice.findObject(new UiSelector().resourceId("com.moviesanywhere.goo:id/poster_image")).click();
        Thread.sleep(1000);
    }

    private void watchOnAmazonPrime() throws UiObjectNotFoundException, InterruptedException {
        mDevice.findObject(new UiSelector().resourceId("com.moviesanywhere.goo:id/button_text").text("BUY")).click();
        if(mDevice.findObject(new UiSelector().resourceId("com.moviesanywhere.goo:id/logo").descriptionContains("Amazon Video")).exists()){
            mDevice.findObject(new UiSelector().resourceId("com.moviesanywhere.goo:id/logo").descriptionContains("Amazon Video")).click();
        }
        if(mDevice.findObject(new UiSelector().resourceId("com.moviesanywhere.goo:id/default_yes").text("Yes")).exists()){
            mDevice.findObject(new UiSelector().resourceId("com.moviesanywhere.goo:id/default_yes").text("Yes")).click();
        }
        Thread.sleep(1000);
    }

    @Override
    protected void playVideo() throws InterruptedException, UiObjectNotFoundException {
        searchVideo();
        watchOnAmazonPrime();
        UiSelector recyclerView = new UiSelector().className("android.webkit.WebView");
        UiScrollable scroll = new UiScrollable(recyclerView);
        scroll.setAsVerticalList();
        scroll.scrollForward(1000);
        Thread.sleep(1000);
        if(mDevice.findObject(new UiSelector().text("Watch From Beginning")).exists()){
            mDevice.findObject(new UiSelector().text("Watch From Beginning")).click();
        }
        //super.playVideo();
        mDevice.pressBack();
        mDevice.pressBack();
        mDevice.pressBack();
    }

    @Override
    protected void signout() throws UiObjectNotFoundException, InterruptedException {
        mDevice.findObject(new UiSelector().descriptionContains("Navigate up")).waitForExists(10000);
        mDevice.findObject(new UiSelector().descriptionContains("Navigate up")).click();
        mDevice.findObject(new UiSelector().resourceId("com.moviesanywhere.goo:id/setting_title").text("Account")).click();
        mDevice.findObject(new UiSelector().resourceId("com.moviesanywhere.goo:id/title").text("Sign Out")).click();
    }

}
