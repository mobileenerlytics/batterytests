package com.mobileenerlytics.uiautomatortests.videoapps;

import android.support.test.runner.AndroidJUnit4;
import android.support.test.uiautomator.BySelector;
import android.support.test.uiautomator.UiObject2;
import android.support.test.uiautomator.UiObjectNotFoundException;
import android.support.test.uiautomator.UiSelector;

import org.junit.After;
import org.junit.Before;
import org.junit.runner.RunWith;

import java.util.List;

import static android.support.test.uiautomator.By.clazz;

/**
 * USAGE:
 *      - Test written for Nexus 6 (1440x2560 pixels)
 *      - Assumes EagleTester installed and setup
 *      - Assumes app installed
 *      - Searches for the song given in SEARCH_TEXT
 *      - User name, password of the app and play time of the video can be passed from the command line
 *        or can be given inside the config.properties file.
 *      - Check the Config class for the app specific properties
 */

@RunWith(AndroidJUnit4.class)
public class Hulu extends BaseVideo {
    private static final String PACKAGE_NAME = "com.hulu.plus";

    @Before
    public void setUpApp() throws Exception {
        super.setUpApp(PACKAGE_NAME,"hulu");
    }

    @Override
    protected void login(String userName, String pwd) throws UiObjectNotFoundException, InterruptedException {
        mDevice.findObject(new UiSelector().resourceId("com.hulu.plus:id/").text("Log in")).click();
        BySelector selector = clazz("android.widget.EditText");
        List<UiObject2> loginPage = mDevice.findObjects(selector);
        loginPage.get(0).setText(userName);
        loginPage.get(1).setText(pwd);
        mDevice.findObject(new UiSelector().resourceId("com.hulu.plus:id/").text("Log in")).click();
    }
    @Override
    protected void searchVideo() throws UiObjectNotFoundException {
        mDevice.findObject(new UiSelector().resourceId("android:id/text1").text("Movies")).click();
    }

    @Override
    protected void playVideo() throws Exception {
        searchVideo();
        mDevice.findObject(new UiSelector().resourceId("com.hulu.plus:id/").descriptionContains("Play")).click();
        super.playVideo();
        mDevice.pressBack();
     }
    @Override
    protected void signout() throws UiObjectNotFoundException, InterruptedException {
        mDevice.findObject(new UiSelector().resourceId("com.hulu.plus:id/").text("account")).click();
        mDevice.findObject(new UiSelector().resourceId("com.hulu.plus:id/").text("Log out of Hulu")).click();
    }


}
