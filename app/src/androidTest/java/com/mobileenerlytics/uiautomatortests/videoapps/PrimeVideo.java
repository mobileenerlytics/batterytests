package com.mobileenerlytics.uiautomatortests.videoapps;

import android.support.test.runner.AndroidJUnit4;
import android.support.test.uiautomator.UiObjectNotFoundException;
import android.support.test.uiautomator.UiScrollable;
import android.support.test.uiautomator.UiSelector;

import org.junit.After;
import org.junit.Before;
import org.junit.runner.RunWith;

/**
 * USAGE:
 *      - Test written for Nexus 6 (1440x2560 pixels)
 *      - Assumes EagleTester installed and setup
 *      - Assumes app installed
 *      - Searches for the song given in SEARCH_TEXT
 *      - User name, password of the app and play time of the video can be passed from the command line
 *        or can be given inside the config.properties file.
 *      - Check the Config class for the app specific properties
 */

@RunWith(AndroidJUnit4.class)
public class PrimeVideo extends BaseVideo {
    private static final String PACKAGE_NAME = "com.amazon.avod.thirdpartyclient";

    private static final String SEARCH_TEXT = "Jack Reacher: Never Go Back";

    @Before
    public void setUpApp() throws Exception {
        super.setUpApp(PACKAGE_NAME, "prime");
    }

    @Override
    protected void login(String userName, String pwd) throws UiObjectNotFoundException, InterruptedException {
        mDevice.findObject(new UiSelector().resourceId("com.amazon.avod.thirdpartyclient:id/LaunchDismissButton").text("Get Started")).click();
        mDevice.findObject(new UiSelector().resourceId("ap_email")).waitForExists(10000);
        mDevice.findObject(new UiSelector().resourceId("ap_email")).setText(userName);
        mDevice.findObject(new UiSelector().resourceId("ap_password")).setText(pwd);
        Thread.sleep(1000);
        mDevice.findObject(new UiSelector().resourceId("signInSubmit")).click();
        Thread.sleep(30000);
    }

    @Override
    protected void searchVideo() throws UiObjectNotFoundException, InterruptedException {
        mDevice.findObject(new UiSelector().resourceId("com.amazon.avod.thirdpartyclient:id/toolbar_search_icon")).waitForExists(60000);
        mDevice.findObject(new UiSelector().resourceId("com.amazon.avod.thirdpartyclient:id/toolbar_search_icon")).click();
        mDevice.findObject(new UiSelector().resourceId("com.amazon.avod.thirdpartyclient:id/search_src_text")).click();
        mDevice.findObject(new UiSelector().resourceId("com.amazon.avod.thirdpartyclient:id/search_src_text")).setText(SEARCH_TEXT);
        Thread.sleep(1000);
        mDevice.findObject(new UiSelector().descriptionContains("Tab ORIGINALS. Double tap to select")).click();
    }

    @Override
    protected void playVideo() throws Exception {
        searchVideo();
        mDevice.findObject(new UiSelector().text(SEARCH_TEXT)).click();
        mDevice.findObject(new UiSelector().resourceId("com.amazon.avod.thirdpartyclient:id/WatchNowBigButton")).click();
        super.playVideo();
        Thread.sleep(1000);
        mDevice.pressBack();
    }

    @Override
    protected void signout() throws UiObjectNotFoundException, InterruptedException {
        mDevice.findObject(new UiSelector().descriptionContains("Navigation panel")).click();
        UiSelector recyclerView = new UiSelector().className("android.support.v7.widget.RecyclerView");
        UiScrollable scroll = new UiScrollable(recyclerView);
        scroll.setAsVerticalList();
        scroll.scrollToEnd(5);
        mDevice.findObject(new UiSelector().resourceId("com.amazon.avod.thirdpartyclient:id/f_primary_text").text("Settings")).click();
        UiSelector frameLayout = new UiSelector().className("android.widget.FrameLayout");
        scroll = new UiScrollable(frameLayout);
        scroll.setAsVerticalList();
        scroll.scrollToEnd(5);
        mDevice.findObject(new UiSelector().resourceId("android:id/summary").text("Sign in with a different Amazon account")).click();
        mDevice.findObject(new UiSelector().resourceId("android:id/button1").text("Sign Out")).click();
    }

}
