package com.mobileenerlytics.uiautomatortests.videoapps;


import android.support.test.runner.AndroidJUnit4;
import android.support.test.uiautomator.UiObject;
import android.support.test.uiautomator.UiObjectNotFoundException;
import android.support.test.uiautomator.UiSelector;

import org.junit.After;
import org.junit.Before;
import org.junit.runner.RunWith;

/**
 * USAGE:
 *      - Test written for Nexus 6 (1440x2560 pixels)
 *      - Assumes EagleTester installed and setup
 *      - Assumes app installed and logged in
 *      - Assumes app installed and logged in
 *      - Searches for the song given in SEARCH_TEXT
 *      - User name, password of the app and play time of the video can be passed from the command line
 *        or can be given inside the config.properties file.
 *      - Check the Config class for the app specific properties
 */

@RunWith(AndroidJUnit4.class)
public class Youtube extends BaseVideo {
    private static final String PACKAGE_NAME = "com.google.android.apps.youtube.music";
    private static final String SEARCH_TEXT = "shawn mendes confession video";

    @Before
    public void before() throws Exception {
        super.setUpApp(PACKAGE_NAME, "youtube");
    }

    @Override
    protected void login(String userName, String pwd) throws UiObjectNotFoundException {
        UiObject notNow = mDevice.findObject(new UiSelector().resourceId("android:id/button2").text("Not now"));
        if(notNow.exists()){
            notNow.click();
        }
    }

    @Override
    protected void searchVideo() throws UiObjectNotFoundException, InterruptedException {
        mDevice.findObject(new UiSelector().resourceId("com.google.android.apps.youtube.music:id/action_search").descriptionContains("Search")).click();
        mDevice.findObject(new UiSelector().resourceId("com.google.android.apps.youtube.music:id/search_edit_text").text("Search music")).waitForExists(10000);
        mDevice.findObject(new UiSelector().resourceId("com.google.android.apps.youtube.music:id/search_edit_text").text("Search music")).setText(SEARCH_TEXT);
        mDevice.findObject(new UiSelector().textContains(SEARCH_TEXT).resourceId("com.google.android.apps.youtube.music:id/text")).waitForExists(10000);
        mDevice.findObject(new UiSelector().textContains(SEARCH_TEXT).resourceId("com.google.android.apps.youtube.music:id/text")).click();
        Thread.sleep(1000);
    }

    @Override
    protected void playVideo() throws Exception {
        searchVideo();
        mDevice.findObject(new UiSelector().resourceId("com.google.android.apps.youtube.music:id/thumbnail")).click();
        Thread.sleep(1000);
        mDevice.findObject(new UiSelector().resourceId("com.google.android.apps.youtube.music:id/enter_fullscreen_button")).click();
        super.playVideo();
        mDevice.click(1000,1000);
        mDevice.findObject(new UiSelector().resourceId("com.google.android.apps.youtube.music:id/player_control_play_pause_replay_button")).click();
        mDevice.findObject(new UiSelector().resourceId("com.google.android.apps.youtube.music:id/player_collapse_button")).click();
        mDevice.findObject(new UiSelector().resourceId("com.google.android.apps.youtube.music:id/player_collapse_button")).click();
        mDevice.findObject(new UiSelector().descriptionContains("Search back")).click();
        mDevice.findObject(new UiSelector().resourceId("com.google.android.apps.youtube.music:id/player_panel_metadata")).swipeRight(20);
    }

    @Override
    protected void signout() throws InterruptedException, UiObjectNotFoundException {

    }

}
