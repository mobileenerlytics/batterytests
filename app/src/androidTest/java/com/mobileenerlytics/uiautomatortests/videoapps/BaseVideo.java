package com.mobileenerlytics.uiautomatortests.videoapps;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.os.Bundle;
import android.support.annotation.CallSuper;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;
import android.support.test.uiautomator.UiDevice;
import android.support.test.uiautomator.Until;

import com.mobileenerlytics.eagle.tester.logger.EagleTester;
import com.mobileenerlytics.uiautomatortests.Config;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.uiautomator.By.pkg;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.assertThat;

/**
 * USAGE:
 *      - Assumes EagleTester installed and setup
 *      - Assumes app installed
 * EXTENDING THIS CLASS:
 *      - See other tests in this package for examples
 *      - Implement login, searchVideo, signOut (every app handles this slightly differently)
 *
 */

@RunWith(AndroidJUnit4.class)
public abstract class BaseVideo {
    protected UiDevice mDevice;
    private static final int LAUNCH_TIMEOUT = 5000;
    private static String userName;
    private static String pwd;
    private static int playTime;
    protected static EagleTester eagleTester;

    @BeforeClass
    public static void setUp() throws InterruptedException {
        eagleTester = new EagleTester(InstrumentationRegistry.getTargetContext());
    }

    public void setUpApp(String packageName, String appName) throws Exception {
        mDevice = UiDevice.getInstance(InstrumentationRegistry.getInstrumentation());
        assertThat(mDevice, notNullValue());
        mDevice.pressHome();
        final String launcherPackage = getLauncherPackageName();
        assertThat(launcherPackage, notNullValue());
        mDevice.wait(Until.hasObject(pkg(launcherPackage).depth(0)), LAUNCH_TIMEOUT);
        final Context context = InstrumentationRegistry.getContext();
        final Intent intent = context.getPackageManager().getLaunchIntentForPackage(packageName);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK); // Clear out any previous instances
        context.startActivity(intent);
        mDevice.wait(Until.hasObject(pkg(packageName).depth(0)), LAUNCH_TIMEOUT);
        Thread.sleep(10000);
        assignProps(appName);
        login(userName,pwd);
    }

    private static String getLauncherPackageName() {
        final Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_HOME);
        PackageManager pm = InstrumentationRegistry.getContext().getPackageManager();
        ResolveInfo resolveInfo = pm.resolveActivity(intent, PackageManager.MATCH_DEFAULT_ONLY);
        return resolveInfo.activityInfo.packageName;
    }

    private static void assignProps(String appName){
        Config.setProps();
        userName = Config.getProps(appName+"Username");
        pwd = Config.getProps(appName+"Password");
        playTime = Integer.parseInt(Config.getProps(appName+"Playtime"));
    }

    protected abstract void login(String userName, String pwd) throws Exception;
    protected abstract void searchVideo() throws Exception;
    protected abstract void signout() throws Exception;

    @CallSuper
    protected void playVideo() throws Exception {
        eagleTester.startMeasure("playVideo");
        Thread.sleep(playTime);
        eagleTester.stopMeasure("playVideo");
    }

    @Test
    public void testPlayVideo() throws Exception {
       playVideo();
    }

    @After
    public void tearDown() throws Exception {
        signout();
    }

    @AfterClass

    public static void destroyEagleTester() throws InterruptedException {
        eagleTester.finish();
    }

}
