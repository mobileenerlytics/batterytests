package com.mobileenerlytics.uiautomatortests.videoapps;

import android.support.test.runner.AndroidJUnit4;
import android.support.test.uiautomator.UiSelector;

import org.junit.Before;
import org.junit.runner.RunWith;

import java.util.List;

/**
 * USAGE:
 *      - Test written for Nexus 6 (1440x2560 pixels)
 *      - Assumes EagleTester installed and setup
 *      - Assumes app installed and logged in
 *      - Searches for the song given in SEARCH_TEXT
 *      - User name, password of the app and play time of the video can be passed from the command line
 *        or can be given inside the config.properties file.
 *      - Check the Config class for the app specific properties
 */

@RunWith(AndroidJUnit4.class)
public class HboGo extends BaseVideo {
    private static final String PACKAGE_NAME = "com.hbo.hbonow";
    private static final String SEARCH_TEXT = "Silicon Valley";

    @Before
    public void before() throws Exception {
        super.setUpApp(PACKAGE_NAME, "hbo");
    }

    @Override
    protected void login(String userName, String pwd) throws Exception {

    }

    @Override
    protected void searchVideo() throws Exception {
        mDevice.findObject(new UiSelector().descriptionContains("Search")).click();
        mDevice.findObject(new UiSelector().text("Search titles…")).click();
        mDevice.findObject(new UiSelector().text("Search titles…")).setText(SEARCH_TEXT);
        mDevice.findObject(new UiSelector().text(SEARCH_TEXT)).click();
    }

    @Override
    protected void playVideo() throws Exception {
        searchVideo();
        Thread.sleep(10000);
        mDevice.findObject(new UiSelector().className("android.widget.ImageView")).click();
        //mDevice.findObject(new UiSelector().className("android.widget.ImageView")).click();
        Thread.sleep(1000);
        super.playVideo();
        mDevice.pressBack();
        mDevice.pressBack();
        mDevice.pressBack();
    }

    @Override
    protected void signout() throws Exception {

    }
}
