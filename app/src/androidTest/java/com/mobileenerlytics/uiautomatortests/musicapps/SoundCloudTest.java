package com.mobileenerlytics.uiautomatortests.musicapps;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;
import android.support.test.uiautomator.UiDevice;
import android.support.test.uiautomator.UiObject;
import android.support.test.uiautomator.UiObjectNotFoundException;
import android.support.test.uiautomator.UiScrollable;
import android.support.test.uiautomator.UiSelector;
import android.support.test.uiautomator.Until;

import com.mobileenerlytics.eagle.tester.logger.EagleTester;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.IOException;

import static android.support.test.uiautomator.By.pkg;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.assertThat;

/**
 * USAGE:
 *      - Test written for Nexus 6 (1440x2560 pixels)
 *      - Assumes EagleTester installed and setup
 *      - Assumes app installed
 *      - Searches for the song given in SEARCH_TEXT
 *      - User name, password of the app and play time of the song can be passed from the command line
 *        or can be given inside the config.properties file.
 *      - Check the Config class for the app specific properties
 */

@RunWith(AndroidJUnit4.class)
public class SoundCloudTest extends BaseMusic {
    private static final String PACKAGE_NAME = "com.soundcloud.android";

    private static final String SEARCH_TEXT = "Chainsmokers Closer";

    @BeforeClass
    public static void setUp() throws InterruptedException {
        eagleTester = new EagleTester(InstrumentationRegistry.getTargetContext());
    }

    @Before
    public void before() throws Exception {
        super.setUpApp(PACKAGE_NAME,"soundcloud");
    }

    @Override
    protected void login(String userName, String pwd) throws Exception {
        mDevice.findObject(new UiSelector().resourceId("com.soundcloud.android:id/btn_login").text("Sign in")).click();
        mDevice.findObject(new UiSelector().resourceId("com.soundcloud.android:id/auto_txt_email_address")).setText(userName);
        mDevice.findObject(new UiSelector().resourceId("com.soundcloud.android:id/txt_password")).setText(pwd);
        mDevice.findObject(new UiSelector().resourceId("com.soundcloud.android:id/btn_login").text("Done")).click();
        Thread.sleep(5000);
        UiObject startListening = mDevice.findObject(new UiSelector().resourceId("com.soundcloud.android:id/btn_go_setup_start"));
        if(startListening.exists()){
            startListening.click();
        }
    }

    @Override
    protected void searchSong() throws Exception {
        mDevice.findObject(new UiSelector().descriptionContains("Search")).click();
        mDevice.findObject(new UiSelector().text("Search SoundCloud")).click();
        mDevice.findObject(new UiSelector().resourceId("com.soundcloud.android:id/search_edit_text")).setText(SEARCH_TEXT);
    }

    @Override
    protected void playSong() throws Exception {
        searchSong();
        mDevice.findObject(new UiSelector().resourceId("com.soundcloud.android:id/search_title")).click();
        mDevice.findObject(new UiSelector().resourceId("com.soundcloud.android:id/list_item_counter")).click();
        mDevice.click(5000,5000);
        mDevice.findObject(new UiSelector().resourceId("com.soundcloud.android:id/player_close_indicator")).click();
        mDevice.pressBack();
        Thread.sleep(1000);
    }

    @Override
    protected void pageFlips() throws Exception {

    }

    @Override
    protected void signout() throws Exception {
        mDevice.findObject(new UiSelector().descriptionContains("More")).click();
        UiSelector frameLayout = new UiSelector().resourceId("com.soundcloud.android:id/profile_header");
        UiScrollable scroll = new UiScrollable(frameLayout);
        scroll.setAsVerticalList();
        scroll.scrollToEnd(5);
        scroll = new UiScrollable(frameLayout);
        scroll.setAsVerticalList();
        scroll.scrollToEnd(5);
        mDevice.findObject(new UiSelector().text("Sign out")).click();
        Thread.sleep(1000);
        mDevice.findObject(new UiSelector().resourceId("android:id/button1").text("OK")).click();
    }

}
