package com.mobileenerlytics.uiautomatortests.musicapps;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;
import android.support.test.uiautomator.BySelector;
import android.support.test.uiautomator.UiDevice;
import android.support.test.uiautomator.UiObject;
import android.support.test.uiautomator.UiObject2;
import android.support.test.uiautomator.UiObjectNotFoundException;
import android.support.test.uiautomator.UiScrollable;
import android.support.test.uiautomator.UiSelector;
import android.support.test.uiautomator.Until;
import android.util.Log;

import com.mobileenerlytics.eagle.tester.logger.EagleTester;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static android.support.test.uiautomator.By.pkg;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.assertThat;

/**
 * USAGE:
 *      - Test written for Nexus 6 (1440x2560 pixels)
 *      - Assumes EagleTester installed and setup
 *      - Assumes app installed
 *      - Searches for the song given in SEARCH_TEXT
 *      - User name, password of the app and play time of the song can be passed from the command line
 *        or can be given inside the config.properties file.
 *      - Check the Config class for the app specific properties
 */

@RunWith(AndroidJUnit4.class)
public class SpotifyTest extends BaseMusic {
    private static final String PACKAGE_NAME = "com.spotify.music";
    private static final String SEARCH_TEXT = "Chainsmokers";

    @Before
    public void before() throws Exception {
        super.setUpApp(PACKAGE_NAME,"spotify");
    }

    @Override
    protected void login(String userName, String pwd) throws Exception {
        UiObject login = mDevice.findObject(new UiSelector().resourceId("com.spotify.music:id/button_login"));
        login.click();
        BySelector selector = pkg(PACKAGE_NAME);
        List<UiObject2> objList = new ArrayList<>();
        UiObject2 loginButton = null;
        List<UiObject2> inputList = new ArrayList<>();
        objList = mDevice.findObjects(selector);
        for(int m=0;m<objList.size();m++){
            //Log.d("propsSpotLogin_25", objList.get(m).toString() + "m:"+m);
            String textbox = objList.get(m).getClassName();
            Log.d("propsSpotLogin_25",textbox);
            if(textbox.equals("android.widget.EditText")){
                inputList.add(objList.get(m));
            }
            if(textbox.equals("android.widget.Button")){
                loginButton = objList.get(m);
            }
        }
        inputList.get(0).setText(userName);
        inputList.get(1).setText(pwd);
        loginButton.click();
    }

    @Override
    protected void searchSong() throws Exception {
        mDevice.findObject(new UiSelector().resourceId("com.spotify.music:id/search_tab")).click();
        Thread.sleep(1000);
        mDevice.findObject(new UiSelector().resourceId("com.spotify.music:id/search_background")).click();
        //mDevice.findObject(new UiSelector().resourceId("com.spotify.music:id/find_search_field")).click();
        mDevice.findObject(new UiSelector().resourceId("com.spotify.music:id/query")).click();
        mDevice.findObject(new UiSelector().resourceId("com.spotify.music:id/query")).setText(SEARCH_TEXT);
        Thread.sleep(1000);
        mDevice.findObject(new UiSelector().resourceId("com.spotify.music:id/labels")).click();
        Thread.sleep(1000);
    }

    @Override
    protected void playSong() throws Exception {
        searchSong();
        mDevice.findObject(new UiSelector().resourceId("com.spotify.music:id/labels")).click();
        Thread.sleep(1000);
        mDevice.findObject(new UiSelector().text("SHUFFLE PLAY")).click();
        mDevice.findObject(new UiSelector().resourceId("com.spotify.music:id/player_navigation_unit_container")).click();
        super.playSong();
        mDevice.findObject(new UiSelector().resourceId("com.spotify.music:id/close_btn")).click();
        mDevice.findObject(new UiSelector().descriptionContains("Back"));
    }

    @Override
    protected void pageFlips() throws Exception {

    }

    @Override
    protected void signout() throws Exception {
        mDevice.findObject(new UiSelector().resourceId("com.spotify.music:id/your_library_tab")).click();
        mDevice.findObject(new UiSelector().descriptionContains("Settings")).click();
        UiSelector listView = new UiSelector().resourceId("android:id/list");
        UiScrollable scroll = new UiScrollable(listView);
        scroll.setAsVerticalList();
        scroll.scrollToEnd(10);
        mDevice.findObject(new UiSelector().resourceId("android:id/text1").text("Log out")).click();
    }

}
