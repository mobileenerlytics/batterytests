package com.mobileenerlytics.uiautomatortests.musicapps;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;
import android.support.test.uiautomator.BySelector;
import android.support.test.uiautomator.UiDevice;
import android.support.test.uiautomator.UiObject;
import android.support.test.uiautomator.UiObject2;
import android.support.test.uiautomator.UiObjectNotFoundException;
import android.support.test.uiautomator.UiScrollable;
import android.support.test.uiautomator.UiSelector;
import android.support.test.uiautomator.Until;
import android.util.Log;

import com.mobileenerlytics.eagle.tester.logger.EagleTester;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static android.support.test.uiautomator.By.pkg;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.assertThat;

/**
 * USAGE:
 *      - Test written for Nexus 6 (1440x2560 pixels)
 *      - Assumes EagleTester installed and setup
 *      - Assumes app installed
 *      - Searches for the song given in SEARCH_TEXT
 *      - User name, password of the app and play time of the song can be passed from the command line
 *        or can be given inside the config.properties file.
 *      - Check the Config class for the app specific properties
 */

@RunWith(AndroidJUnit4.class)
public class AmazonMusic extends BaseMusic {
    private static final String PACKAGE_NAME = "com.amazon.mp3";

    private static final String SEARCH_TEXT = "closer chainsmokers";

    @Before
    public void before() throws Exception {
        super.setUpApp(PACKAGE_NAME,"amazonmusic");
    }

    @Override
    protected void login(String userName, String pwd) throws Exception {
        UiObject getStarted = mDevice.findObject(new UiSelector().resourceId("com.amazon.avod.thirdpartyclient:id/LaunchDismissButton").text("Get Started"));
        if(getStarted.exists()){
            getStarted.click();
        }
        mDevice.findObject(new UiSelector().resourceId("ap_email")).waitForExists(10000);
        mDevice.findObject(new UiSelector().resourceId("ap_email")).setText(userName);
        mDevice.findObject(new UiSelector().resourceId("ap_password")).setText(pwd);
        Thread.sleep(1000);
        mDevice.findObject(new UiSelector().resourceId("signInSubmit")).click();
        Thread.sleep(30000);
        BySelector selector = pkg(PACKAGE_NAME);
        List<UiObject2> objList = new ArrayList<>();
        objList = mDevice.findObjects(selector);
        objList.get(1).click();
        Log.d("startListening",objList.get(1).getClassName());
        UiObject notNow = mDevice.findObject(new UiSelector().resourceId("com.amazon.mp3:id/app_rating_not_now_button").text("Not Now"));
        if(notNow.exists()){
            notNow.click();
        }
    }

    @Override
    protected void searchSong() throws Exception {
        mDevice.findObject(new UiSelector().resourceId("com.amazon.mp3:id/Search")).click();
        mDevice.findObject(new UiSelector().resourceId("android:id/search_src_text")).setText(SEARCH_TEXT);
        Thread.sleep(500);
        mDevice.findObject(new UiSelector().resourceId("com.amazon.mp3:id/tab_view_0").text("My Music")).click();
    }

    @Override
    public void playSong() throws Exception {
        searchSong();
        UiObject gotIt = mDevice.findObject(new UiSelector().text("GOT IT"));
        if(gotIt.exists()){
            gotIt.click();
        }
        mDevice.findObject(new UiSelector().resourceId("com.amazon.mp3:id/AlbumArtwork")).click();
        Thread.sleep(1000);
        mDevice.findObject(new UiSelector().resourceId("com.amazon.mp3:id/PlayButton")).click();
        super.playSong();
        mDevice.pressBack();
        mDevice.pressBack();
        mDevice.pressBack();
        Thread.sleep(1000);
    }

    @Override
    protected void pageFlips() throws UiObjectNotFoundException, InterruptedException {
    }

    @Override
    protected void signout() throws Exception {
        mDevice.findObject(new UiSelector().descriptionContains("Navigate up")).click();
        UiSelector frameLayout = new UiSelector().className("android.widget.FrameLayout");
        UiScrollable scroll = new UiScrollable(frameLayout);
        scroll.setAsVerticalList();
        scroll.scrollToEnd(5);
        scroll = new UiScrollable(frameLayout);
        scroll.setAsVerticalList();
        scroll.scrollToEnd(5);
        mDevice.findObject(new UiSelector().resourceId("android:id/title").text("Sign Out")).click();
        mDevice.findObject(new UiSelector().resourceId("android:id/button1").text("Sign Out")).click();
    }

}
