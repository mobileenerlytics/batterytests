package com.mobileenerlytics.uiautomatortests.musicapps;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;
import android.support.test.uiautomator.UiDevice;
import android.support.test.uiautomator.UiObject;
import android.support.test.uiautomator.UiObjectNotFoundException;
import android.support.test.uiautomator.UiSelector;
import android.support.test.uiautomator.Until;

import com.mobileenerlytics.eagle.tester.logger.EagleTester;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.IOException;

import static android.support.test.uiautomator.By.pkg;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.assertThat;

/**
 * USAGE:
 *      - Test written for Nexus 6 (1440x2560 pixels)
 *      - Assumes EagleTester installed and setup
 *      - Assumes app installed
 *      - Searches for the song given in SEARCH_TEXT
 *      - User name, password of the app and play time of the song can be passed from the command line
 *        or can be given inside the config.properties file.
 *      - Check the Config class for the app specific properties
 */

@RunWith(AndroidJUnit4.class)
public class TuneinTest extends BaseMusic {
    private static final String PACKAGE_NAME = "tunein.player";
    private static final String SEARCH_TEXT = "Coldplay";

    @Before
    public void before() throws Exception {
        super.setUpApp(PACKAGE_NAME,"tunein");
    }

    @Override
    protected void login(String userName, String pwd) throws Exception {
      UiObject allowBtn = mDevice.findObject(new UiSelector().resourceId("com.android.packageinstaller:id/permission_allow_button"));
      if(allowBtn.exists()){
          allowBtn.click();
      }
      if(allowBtn.exists()){
          allowBtn.click();
      }
      UiObject none = mDevice.findObject(new UiSelector().resourceId("com.google.android.gms:id/cancel").text("None of the above"));
      if(none.exists()){
          none.click();
      }
      /*mDevice.findObject(new UiSelector().descriptionContains("Open navigation drawer")).click();
      mDevice.findObject(new UiSelector().resourceId("tunein.player:id/design_menu_item_text").text("Settings")).click();
      UiObject signIn = mDevice.findObject(new UiSelector().resourceId("tunein.player:id/button_text").text("Sign in or sign up"));
      if(signIn.exists()){
          signIn.click();
          if(none.exists()){
              none.click();
          }
          mDevice.findObject(new UiSelector().resourceId("tunein.player:id/fragment_reg_wall_sign_in").text("Sign In.")).click();
          mDevice.findObject(new UiSelector().resourceId("tunein.player:id/emailAddress")).setText(User_Name);
          mDevice.findObject(new UiSelector().resourceId("tunein.player:id/password")).setText(Pwd);
          mDevice.findObject(new UiSelector().resourceId("tunein.player:id/next").text("Submit")).click();
      }
      mDevice.findObject(new UiSelector().descriptionContains("Navigate up")).click();*/
    }

    @Override
    protected void searchSong() throws Exception {
        mDevice.findObject(new UiSelector().resourceId("tunein.player:id/action_bar_search")).click();
        mDevice.findObject(new UiSelector().resourceId("tunein.player:id/search_bar")).click();
        mDevice.findObject(new UiSelector().resourceId("tunein.player:id/search_src_text")).setText(SEARCH_TEXT);
        Thread.sleep(5000);
    }

    @Override
    protected void playSong() throws Exception {
        searchSong();
        Thread.sleep(1000);
        mDevice.findObject(new UiSelector().resourceId("tunein.player:id/row_square_cell_title")).click();
        mDevice.findObject(new UiSelector().resourceId("tunein.player:id/profile_primary_button")).click();
        UiObject miniPlayer = mDevice.findObject(new UiSelector().resourceId("tunein.player:id/mini_player_button_play"));
        if(miniPlayer.exists()){
            miniPlayer.click();
        }
        super.playSong();
        mDevice.findObject(new UiSelector().descriptionContains("Navigate up")).click();
        mDevice.findObject(new UiSelector().descriptionContains("Navigate up")).click();
        mDevice.findObject(new UiSelector().descriptionContains("Navigate up")).click();
        Thread.sleep(10000);
    }

    @Override
    protected void pageFlips() throws Exception {

    }

    @Override
    protected void signout() throws Exception {
       /* mDevice.findObject(new UiSelector().descriptionContains("Open navigation drawer")).click();
        mDevice.findObject(new UiSelector().resourceId("tunein.player:id/design_menu_item_text").text("Settings")).click();
        mDevice.findObject(new UiSelector().resourceId("tunein.player:id/button_text").text("Sign out")).click();
        mDevice.findObject(new UiSelector().resourceId("android:id/button1").text("OK")).click();
        UiObject none = mDevice.findObject(new UiSelector().resourceId("com.google.android.gms:id/cancel").text("None of the above"));
        if(none.exists()){
            none.click();
        }
        mDevice.findObject(new UiSelector().resourceId("tunein.player:id/close_button")).click();*/
    }
}
