package com.mobileenerlytics.uiautomatortests.musicapps;

import android.support.test.runner.AndroidJUnit4;
import android.support.test.uiautomator.By;
import android.support.test.uiautomator.BySelector;
import android.support.test.uiautomator.UiObject;
import android.support.test.uiautomator.UiObject2;
import android.support.test.uiautomator.UiObjectNotFoundException;
import android.support.test.uiautomator.UiScrollable;
import android.support.test.uiautomator.UiSelector;

import org.junit.Before;
import org.junit.runner.RunWith;

import java.util.List;

/**
 * USAGE:
 *      - Test written for Nexus 6 (1440x2560 pixels)
 *      - Assumes EagleTester installed and setup
 *      - Assumes app installed
 *      - Searches for the song given in SEARCH_TEXT
 *      - User name, password of the app and play time of the song can be passed from the command line
 *        or can be given inside the config.properties file.
 *      - Check the Config class for the app specific properties
 */

@RunWith(AndroidJUnit4.class)
public class PandoraTest extends BaseMusic {

    private static final String PACKAGE_NAME = "com.pandora.android";
    private static final String SEARCH_TEXT = "perfect";

    @Before
    public void before() throws Exception {
        super.setUpApp(PACKAGE_NAME,"pandora");
    }

    @Override
    protected void login(String userName, String pwd) throws Exception {
        UiObject login = mDevice.findObject(new UiSelector().resourceId("com.pandora.android:id/welcome_log_in_button"));
        if(login.exists()){
            login.click();
        }
        UiObject signIn = mDevice.findObject(new UiSelector().resourceId("com.pandora.android:id/button_sign_in_submit"));
        if(signIn.exists()){
            BySelector selector = By.res("com.pandora.android:id/input_field");
            List<UiObject2> list = mDevice.findObjects(selector);
            list.get(0).setText(userName);
            list.get(1).setText(pwd);
            signIn.click();
            Thread.sleep(1000);
            UiObject getStarted = mDevice.findObject(new UiSelector().resourceId("com.pandora.android:id/get_started_button"));
            if(getStarted.exists()){
                getStarted.click();
            }
        }
    }

    @Override
    protected void searchSong() throws Exception {
        mDevice.findObject(new UiSelector().resourceId("com.pandora.android:id/search_action")).click();
        mDevice.findObject(new UiSelector().resourceId("com.pandora.android:id/search_src_text")).setText(SEARCH_TEXT);
    }

    @Override
    protected void playSong() throws Exception {
        searchSong();
        mDevice.findObject(new UiSelector().resourceId("com.pandora.android:id/auxiliary_button").descriptionContains("Play")).click();
        mDevice.findObject(new UiSelector().resourceId("com.pandora.android:id/mini_player_content")).click();
        super.playSong();
        mDevice.findObject(new UiSelector().resourceId("com.pandora.android:id/play")).click();
        mDevice.findObject(new UiSelector().descriptionContains("Minimize Now Playing")).click();
        mDevice.findObject(new UiSelector().resourceId("com.pandora.android:id/toolbar_home")).click();
        mDevice.findObject(new UiSelector().resourceId("com.pandora.android:id/toolbar_home")).click();
    }

    @Override
    protected void signout() throws Exception {
        mDevice.findObject(new UiSelector().resourceId("com.pandora.android:id/toolbar_home")).click();
        mDevice.findObject(new UiSelector().descriptionContains("Settings")).click();
        UiSelector linearLayout = new UiSelector().className("android.widget.LinearLayout");
        UiScrollable scroll = new UiScrollable(linearLayout);
        scroll.setAsVerticalList();
        scroll.scrollToEnd(5);
        mDevice.findObject(new UiSelector().resourceId("com.pandora.android:id/signout_button")).click();
        mDevice.findObject(new UiSelector().resourceId("android:id/button1").text("Sign Out")).click();
    }

    @Override
    protected void pageFlips() throws Exception {
        searchSong();
        mDevice.findObject(new UiSelector().resourceId("com.pandora.android:id/auxiliary_button").descriptionContains("Play")).click();
        mDevice.findObject(new UiSelector().resourceId("com.pandora.android:id/toolbar_home")).click();

        UiObject toolBar = mDevice.findObject(new UiSelector().resourceId("com.pandora.android:id/toolbar_home"));
        Thread.sleep(1000);
        toolBar.click();

        Thread.sleep(1000);
        UiObject feed = mDevice.findObject(new UiSelector().descriptionContains("Feed"));
        feed.click();

        Thread.sleep(1000);
        toolBar.click();

        Thread.sleep(1000);
        UiObject profile = mDevice.findObject(new UiSelector().descriptionContains("Profile"));
        profile.click();

        Thread.sleep(1000);
        toolBar.click();

        Thread.sleep(1000);
        UiObject settings = mDevice.findObject(new UiSelector().descriptionContains("Settings"));
        settings.click();
        Thread.sleep(1000);
    }

}
