package com.mobileenerlytics.uiautomatortests.calculator;

import android.support.test.uiautomator.UiSelector;

import org.junit.Before;

import java.util.HashMap;

public class SamsungCalculator extends BaseCalculator{
    private static final String PACKAGE_NAME = "com.sec.android.app.popupcalculator";
    private static final String BASE_RESOURCE_ID = PACKAGE_NAME + ":id/";

    @Before
    public void before() throws InterruptedException {
        super.setUpApp(PACKAGE_NAME);
        map = new HashMap<>();
        map.put('+',BASE_RESOURCE_ID + "bt_add");
        map.put('*',BASE_RESOURCE_ID + "bt_mul");
        map.put('/',BASE_RESOURCE_ID + "bt_div");
        map.put('-',BASE_RESOURCE_ID + "bt_sub");
        map.put('=',BASE_RESOURCE_ID + "clear");
        map.put('c',BASE_RESOURCE_ID + "equal");
        for(int i = 0 ; i <= 9; i ++) {
            char a = (char) (i + '0');
            map.put(new Character(a), BASE_RESOURCE_ID + "bt_" + i);
        }

    }
}
