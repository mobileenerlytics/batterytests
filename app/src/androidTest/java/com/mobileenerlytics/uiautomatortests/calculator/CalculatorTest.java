package com.mobileenerlytics.uiautomatortests.calculator;

import android.support.test.runner.AndroidJUnit4;
import android.support.test.uiautomator.UiSelector;
import android.util.Log;

import org.junit.Before;
import org.junit.runner.RunWith;

import java.util.HashMap;

@RunWith(AndroidJUnit4.class)
public class CalculatorTest extends BaseCalculator {
    private static final String PACKAGE_NAME = "com.google.android.calculator";
    private static final String BASE_RESOURCE_ID = PACKAGE_NAME + ":id/";

    @Before
    public void before() throws InterruptedException {
        super.setUpApp(PACKAGE_NAME);
        map = new HashMap<>();
        map.put('+',BASE_RESOURCE_ID  + "op_add");
        map.put('*',BASE_RESOURCE_ID  + "op_mul");
        map.put('/',BASE_RESOURCE_ID  + "op_div");
        map.put('-',BASE_RESOURCE_ID  + "op_sub");
        map.put('=',BASE_RESOURCE_ID  + "eq");
        map.put('c',BASE_RESOURCE_ID  + "clr");
        for(int i = 0 ; i <= 9; i ++) {
            char a = (char)(i + '0');
            map.put(new Character(a), BASE_RESOURCE_ID  + "digit_" + i);
        }
    }
}
