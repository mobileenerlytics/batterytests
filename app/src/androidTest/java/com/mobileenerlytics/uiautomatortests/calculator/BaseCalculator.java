package com.mobileenerlytics.uiautomatortests.calculator;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.support.annotation.CallSuper;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;
import android.support.test.uiautomator.By;
import android.support.test.uiautomator.UiDevice;
import android.support.test.uiautomator.UiObjectNotFoundException;
import android.support.test.uiautomator.UiSelector;
import android.support.test.uiautomator.Until;
import android.util.Log;

import com.mobileenerlytics.eagle.tester.logger.EagleTester;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.HashMap;

import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.assertThat;

@RunWith(AndroidJUnit4.class)
public abstract class BaseCalculator {
    protected UiDevice mDevice;
    private static final int LAUNCH_TIMEOUT = 5000;
    protected static EagleTester eagleTester;
    protected HashMap<Character,String> map;

    @BeforeClass
    public static void setUp() throws InterruptedException {
        eagleTester = new EagleTester(InstrumentationRegistry.getTargetContext());
    }

    public void setUpApp(String packageName) throws InterruptedException {
        // Initialize UiDevice instance
        mDevice = UiDevice.getInstance(InstrumentationRegistry.getInstrumentation());
        assertThat(mDevice, notNullValue());
        mDevice.pressHome();

        final String launcherPackage = getLauncherPackageName();
        assertThat(launcherPackage, notNullValue());
        mDevice.wait(Until.hasObject(By.pkg(launcherPackage).depth(0)), LAUNCH_TIMEOUT);

        // Launch the blueprint app
        Context context = InstrumentationRegistry.getContext();
        final Intent intent = context.getPackageManager()
                .getLaunchIntentForPackage(packageName);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);    // Clear out any previous instances
        context.startActivity(intent);

        // Wait for the app to appear
        mDevice.wait(Until.hasObject(By.pkg(packageName).depth(0)), LAUNCH_TIMEOUT);
    }

    private String getLauncherPackageName() {
        // Create launcher Intent
        final Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_HOME);

        // Use PackageManager to get the launcher package name
        PackageManager pm = InstrumentationRegistry.getContext().getPackageManager();
        ResolveInfo resolveInfo = pm.resolveActivity(intent, PackageManager.MATCH_DEFAULT_ONLY);
        return resolveInfo.activityInfo.packageName;
    }

    protected void clickInput(String val) throws UiObjectNotFoundException {
        char[] arr = val.toCharArray();
        for(int i=0;i<val.length();i++) {
            Log.d("mapval",map.get(new Character(arr[i]))+"hello");
            mDevice.findObject(new UiSelector().resourceId(map.get(new Character(arr[i])))).click();
        }
    }

    @Test
    public void calcTestA() throws Exception {
        eagleTester.startMeasure("calcTestA");
        Thread.sleep(1000);
        clickInput("12345+6789=c");
        clickInput("12345*6789=c");
        eagleTester.stopMeasure("calcTestA");
    }

    @Test
    public void calcTestB() throws Exception {
        eagleTester.startMeasure("calcTestB");
        Thread.sleep(1000);
        clickInput("12345/6789=c");
        clickInput("12345-6789=c");
        eagleTester.stopMeasure("calcTestB");
    }

    @AfterClass
    public static void destroyEagleTester() throws InterruptedException {
        eagleTester.finish();
    }


}
