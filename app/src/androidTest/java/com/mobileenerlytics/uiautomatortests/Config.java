package com.mobileenerlytics.uiautomatortests;


import android.os.Bundle;
import android.support.test.InstrumentationRegistry;

import java.io.InputStream;
import java.util.HashMap;
import java.util.Properties;
import java.util.Set;

/**
 * USAGE:
 *      - Assumes config.properties file is created(Check config.properties.example file for the property names syntax)
 *      - Checks the command line arguments for the properties. If not found loads them from the config.properties file.
 *      - Check the config.properties.example file for the keys that can be passed as arguments from the command line
 *      - App name is passed as an argument to the setProps to load the app specific properties.
 *      - Example command with arguments:
 *        adb shell am instrument -w -r   -e debug false -e class com.mobileenerlytics.uiautomatortests.musicapps.PandoraTest -e pandoraUsername user@example.com -e pandoraPassword password -e pandoraPlaytime 60000 com.mobileenerlytics.uiautomatortests.test/android.support.test.runner.AndroidJUnitRunner
 */

public class Config {
    static Bundle bundle;
    static Properties props;
    public static void setProps(){
        try (InputStream is = InstrumentationRegistry.getContext().getResources().getAssets().
                open("config.properties")) {
            props = new Properties();
            props.load(is);
            bundle = InstrumentationRegistry.getArguments();
        } catch (Exception e) {
        }
    }

    public static String getProps(String key){
        if(bundle.containsKey(key)){
            return bundle.getString(key);
        }
        return props.getProperty(key);
    }
}
