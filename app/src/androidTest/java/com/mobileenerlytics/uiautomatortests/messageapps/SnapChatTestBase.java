package com.mobileenerlytics.uiautomatortests.messageapps;

import android.support.test.filters.SdkSuppress;
import android.support.test.runner.AndroidJUnit4;
import android.support.test.uiautomator.UiObjectNotFoundException;

import org.junit.Before;
import org.junit.runner.RunWith;

/**
 * Created by srainha on 16/04/18.
 *
 * USAGE:
 *      - Currently UNSUPPORTED because Snapchat blocks rooted phones
 */

@RunWith(AndroidJUnit4.class)
@SdkSuppress(minSdkVersion = 18)
public class SnapChatTestBase extends BaseMessageApps {

    private static final String PACKAGE_NAME = "com.snapchat";

    @Before
    @Override
    public void setupChat() throws UiObjectNotFoundException, InterruptedException {
        super.setupApp(PACKAGE_NAME,"snapchat");
//        mDevice.findObject(new UiSelector().resourceId("com.bsb.hike:id/conversation_item")).click();
//        messageBox = mDevice.findObject(new UiSelector().resourceId("com.bsb.hike:id/msg_compose"));
//        sendButton = mDevice.findObject(new UiSelector().resourceId("com.bsb.hike:id/attachment_view"));
    }

    @Override
    protected void sendImage() throws UiObjectNotFoundException {
    }
}
