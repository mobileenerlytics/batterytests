package com.mobileenerlytics.uiautomatortests.messageapps;

import android.support.test.filters.SdkSuppress;
import android.support.test.runner.AndroidJUnit4;
import android.support.test.uiautomator.UiObjectNotFoundException;
import android.support.test.uiautomator.UiScrollable;
import android.support.test.uiautomator.UiSelector;

import com.mobileenerlytics.uiautomatortests.Config;

import org.junit.Before;
import org.junit.runner.RunWith;

/**
 * Created by srainha on 16/04/18.
 *
 * USAGE:
 *      - Test written for Nexus 6 (1440x2560 pixels)
 *      - Assumes EagleTester installed and setup
 *      - Assumes app installed
 *      - Assumes logged in and a chat exists
 *      - Set the facebookImageName property value in the config.properties file with the name of the image
 */

@RunWith(AndroidJUnit4.class)
@SdkSuppress(minSdkVersion = 18)
public class FbMessengerTestBase extends BaseMessageApps {

    @Before
    @Override
    public void setupChat() throws UiObjectNotFoundException, InterruptedException {
        super.setupApp("com.facebook.orca", "facebook");
        mDevice.findObject(new UiSelector().descriptionContains("Search")).click();
        mDevice.findObject(new UiSelector().text(Config.getProps("facebookUsername"))).click();
        messageBox = mDevice.findObject(new UiSelector().text("Aa"));
        sendButton = mDevice.findObject(new UiSelector().description("Send"));
    }

    private void findTextScroll(String searchText) throws UiObjectNotFoundException {
        UiScrollable textScroll = new UiScrollable(new UiSelector().scrollable(true));
        textScroll.scrollIntoView(new UiSelector().description(searchText));
        mDevice.findObject(new UiSelector().description(searchText)).click();
    }

    @Override
    protected void sendImage() throws UiObjectNotFoundException {
        mDevice.findObject(new UiSelector().description("Choose photo")).click();
        mDevice.findObject(new UiSelector().description("Media Gallery")).click();
        findTextScroll(Config.getProps("facebookImageName"));
        mDevice.findObject(new UiSelector().description("Next")).click();
    }

}
