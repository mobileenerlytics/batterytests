package com.mobileenerlytics.uiautomatortests.messageapps;

import android.support.test.filters.SdkSuppress;
import android.support.test.runner.AndroidJUnit4;
import android.support.test.uiautomator.UiObjectNotFoundException;
import android.support.test.uiautomator.UiSelector;

import org.junit.Before;
import org.junit.runner.RunWith;

/**
 * Created by srainha on 16/04/18.
 *
 * USAGE:
 *      - Test written for Nexus 6 (1440x2560 pixels)
 *      - Assumes EagleTester installed and setup
 *      - Assumes app installed
 *      - Assumes logged in and a chat exists
 */

@RunWith(AndroidJUnit4.class)
@SdkSuppress(minSdkVersion = 18)
public class GoogleHangoutsTestBase extends BaseMessageApps {

    @Before
    @Override
    public void setupChat() throws UiObjectNotFoundException, InterruptedException {
        super.setupApp("com.google.android.talk", "hangouts");
        mDevice.findObject(new UiSelector().resourceId("com.google.android.talk:id/conversationContent")).click();
        messageBox = mDevice.findObject(new UiSelector().text("Write a message"));
        sendButton = mDevice.findObject(new UiSelector().resourceId("com.google.android.talk:id/floating_send_button"));
    }

    @Override
    protected void sendImage() throws UiObjectNotFoundException {
        mDevice.findObject(new UiSelector().resourceId("com.google.android.talk:id/gallerypicker_indicator_icon")).click();
        mDevice.findObject(new UiSelector().resourceId("com.google.android.talk:id/gallery_item_image")).click();
        mDevice.findObject(new UiSelector().resourceId("com.google.android.talk:id/floating_send_button"));
    }

}
