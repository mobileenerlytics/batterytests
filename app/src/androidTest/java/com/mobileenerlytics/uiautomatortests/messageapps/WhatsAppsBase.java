package com.mobileenerlytics.uiautomatortests.messageapps;

import android.support.test.filters.SdkSuppress;
import android.support.test.runner.AndroidJUnit4;
import android.support.test.uiautomator.UiObjectNotFoundException;
import android.support.test.uiautomator.UiSelector;

import org.junit.Before;
import org.junit.runner.RunWith;

/**
 * Created by srainha on 16/04/18.
 *
 * USAGE:
 *      - Test written for Nexus 6 (1440x2560 pixels)
 *      - Assumes EagleTester installed and setup
 *      - Assumes app installed
 *      - Assumes logged in and a chat exists
 *      - Note: This test selects the top (most recent) conversation
 *      - Note: Will select the most recent image in the Download folder
 */

@RunWith(AndroidJUnit4.class)
@SdkSuppress(minSdkVersion = 18)
public class WhatsAppsBase extends BaseMessageApps {

    @Before
    @Override
    public void setupChat() throws UiObjectNotFoundException, InterruptedException {
        super.setupApp("com.whatsapp","whatsapp");
        mDevice.findObject(new UiSelector().text("Chat")).click();
        messageBox = mDevice.findObject(new UiSelector().text("Type a message"));
        sendButton = mDevice.findObject(new UiSelector().description("Send"));
    }

    @Override
    protected void sendImage() throws UiObjectNotFoundException {
        mDevice.findObject(new UiSelector().description("Attach")).click();
        mDevice.findObject(new UiSelector().description("Gallery")).click();
        mDevice.findObject(new UiSelector().text("Download")).click();
        mDevice.findObject(new UiSelector().description("Photo")).click();
        mDevice.findObject(new UiSelector().description("Send")).click();
    }

}
