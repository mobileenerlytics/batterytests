package com.mobileenerlytics.uiautomatortests.messageapps;

import android.support.test.filters.SdkSuppress;
import android.support.test.runner.AndroidJUnit4;
import android.support.test.uiautomator.UiObjectNotFoundException;
import android.support.test.uiautomator.UiSelector;

import com.mobileenerlytics.uiautomatortests.Config;

import org.junit.Before;
import org.junit.runner.RunWith;

/**
 * Created by srainha on 16/04/18.
 *
 * USAGE:
 *      - Test written for Nexus 6 (1440x2560 pixels)
 *      - Assumes EagleTester installed and setup
 *      - Assumes app installed
 *      - Assumes logged in and a chat exists
 *      - Replace the X/Y COORDINATES with the screen location of the SEND ATTACHMENT button.
 *      - Replace telegramImageIndex property with the index of the image (in Downloads) to send.
 *      - Note: This will pick the top (most recent) conversation
 */

@RunWith(AndroidJUnit4.class)
@SdkSuppress(minSdkVersion = 18)
public class TelegramTestBase extends BaseMessageApps {

    private final int X_COORD = 1200;
    private final int Y_COORD = 2300;

    @Before
    @Override
    public void setupChat() throws UiObjectNotFoundException, InterruptedException {
        super.setupApp("org.telegram.messenger","telegram");
        mDevice.findObject(new UiSelector().className("android.view.ViewGroup")).click();
        messageBox = mDevice.findObject(new UiSelector().className("android.widget.EditText"));
    }

    @Override
    void sendTextMessage(String message) throws UiObjectNotFoundException{
        messageBox.setText(message);
        mDevice.pressEnter();
        messageBox.waitForExists(UI_DELAY);
    }

    @Override
    protected void sendImage() throws UiObjectNotFoundException {
        mDevice.click(X_COORD, Y_COORD);
        mDevice.findObject(new UiSelector().text("Gallery")).click();
        mDevice.findObject(new UiSelector().text("Download")).click();
        mDevice.findObject(new UiSelector().index(Integer.parseInt(Config.getProps("telegramImageIndex")))).click();
        mDevice.findObject(new UiSelector().text("SEND")).click();
    }

}
