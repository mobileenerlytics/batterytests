package com.mobileenerlytics.uiautomatortests.messageapps;

import android.support.test.filters.SdkSuppress;
import android.support.test.runner.AndroidJUnit4;
import android.support.test.uiautomator.UiObjectNotFoundException;
import android.support.test.uiautomator.UiSelector;

import com.mobileenerlytics.uiautomatortests.Config;

import org.junit.Before;
import org.junit.runner.RunWith;

/**
 * Created by srainha on 16/04/18.
 *
 * USAGE:
 *      - Test written for Nexus 6 (1440x2560 pixels)
 *      - Assumes EagleTester installed and setup
 *      - Assumes app installed
 *      - Assumes logged in and a chat exists
 *      - Set the viberChatName property in the config.properties file with the name of the chat
 *      - Replace IMAGE_NAME to send a specific picture, otherwise it will send most recent.
 *      - Note: Image picked is the most recent downloaded picture.
 *      - Note: Viber requires 2 accounts to test (can't message self)
 */

@RunWith(AndroidJUnit4.class)
@SdkSuppress(minSdkVersion = 18)
public class ViberTestBase extends BaseMessageApps {

    private static final String PACKAGE_NAME = "com.viber.voip";

    @Before
    @Override
    public void setupChat() throws UiObjectNotFoundException, InterruptedException {
        super.setupApp(PACKAGE_NAME,"viber");
        mDevice.findObject(new UiSelector().text(Config.getProps("viberChatName"))).click();
        messageBox = mDevice.findObject(new UiSelector().text("Type a message…"));
        sendButton = mDevice.findObject(new UiSelector().resourceId("com.viber.voip:id/send_icon_container"));
    }

    @Override
    protected void sendImage() throws UiObjectNotFoundException {
        mDevice.findObject(new UiSelector().resourceId("com.viber.voip:id/options_menu_open_gallery")).click();
        mDevice.findObject(new UiSelector().resourceId("com.viber.voip:id/open_gallery")).click();
        mDevice.findObject(new UiSelector().text("Download")).click();
        mDevice.findObject(new UiSelector().resourceId("com.viber.voip:id/image")).click();
        mDevice.findObject(new UiSelector().resourceId("com.viber.voip:id/menu_done")).click();
        mDevice.findObject(new UiSelector().resourceId("com.viber.voip:id/btn_send")).click();
    }

}
