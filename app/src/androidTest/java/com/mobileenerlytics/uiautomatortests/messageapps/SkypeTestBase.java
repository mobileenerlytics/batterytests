package com.mobileenerlytics.uiautomatortests.messageapps;

import android.support.test.filters.SdkSuppress;
import android.support.test.runner.AndroidJUnit4;
import android.support.test.uiautomator.UiObjectNotFoundException;
import android.support.test.uiautomator.UiSelector;

import com.mobileenerlytics.uiautomatortests.Config;

import org.junit.Before;
import org.junit.runner.RunWith;

/**
 * Created by srainha on 16/04/18.
 *
 * USAGE:
 *      - Test written for Nexus 6 (1440x2560 pixels)
 *      - Assumes EagleTester installed and setup
 *      - Assumes app installed
 *      - Assumes logged in and a chat exists, and you are IN the chat before the test begins
 *      - Set skypeGroup property value in Config.properties with the name of the chat
 */

@RunWith(AndroidJUnit4.class)
@SdkSuppress(minSdkVersion = 18)
public class SkypeTestBase extends BaseMessageApps {

    private static final String PACKAGE_NAME = "com.skype.raider";
    private static final int SEND_X_COORD = 1250;
    private static final int SEND_Y_COORD = 2150;

    @Before
    @Override
    public void setupChat() throws UiObjectNotFoundException, InterruptedException {
        super.setupApp(PACKAGE_NAME, "skype");
//        mDevice.findObject(new UiSelector().descriptionContains(Config.getProps("skypeGroup")))).click();
        messageBox = mDevice.findObject(new UiSelector().text("Type a message"));
        sendButton = mDevice.findObject(new UiSelector().description("Send message"));
    }

    @Override
    void sendTextMessage(String message) throws UiObjectNotFoundException{
        messageBox.setText(message);
        sendButton.waitForExists(UI_DELAY);
        sendButton.click();
    }

    @Override
    protected void sendImage() throws UiObjectNotFoundException {
        mDevice.findObject(new UiSelector().description("Add to chat")).click();
        mDevice.findObject(new UiSelector().description("Add media or files")).click();
        mDevice.findObject(new UiSelector().description("Gallery, Tap to change")).click();
        mDevice.findObject(new UiSelector().text("Download")).click();
        mDevice.findObject(new UiSelector().description("Gallery item")).click();
        mDevice.findObject(new UiSelector().description("Next")).click();
        mDevice.findObject(new UiSelector().text(Config.getProps("skypeGroup"))).waitForExists(UI_DELAY);
        mDevice.click(SEND_X_COORD, SEND_Y_COORD);
        messageBox.waitForExists(UI_DELAY);
    }

}
