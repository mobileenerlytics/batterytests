package com.mobileenerlytics.uiautomatortests.messageapps;

import android.support.test.filters.SdkSuppress;
import android.support.test.runner.AndroidJUnit4;
import android.support.test.uiautomator.UiObjectNotFoundException;
import android.support.test.uiautomator.UiSelector;

import com.mobileenerlytics.uiautomatortests.Config;

import org.junit.Before;
import org.junit.runner.RunWith;

/**
 * Created by srainha on 16/04/18.
 *
 * USAGE:
 *      - Test written for Nexus 6 (1440x2560 pixels)
 *      - Assumes EagleTester installed and setup
 *      - Assumes app installed
 *      - Assumes logged in and a chat exists
 *      - Set the value of the wechatChatName in config.properties with the name of the chat
 *      - Replace the SEND_COORDINATES with the screen location of the SEND button
 *      - Set the value of the wechatImageName in config.properties to send a specific picture, otherwise it will send most recent.
 */

@RunWith(AndroidJUnit4.class)
@SdkSuppress(minSdkVersion = 18)
public class WeChatTestBase extends BaseMessageApps {

    private final String PACKAGE_NAME = "com.tencent.mm";
    private final int SEND_COORD_X = 1300;
    private final int SEND_COORD_Y = 2300;


    @Before
    @Override
    public void setupChat() throws UiObjectNotFoundException, InterruptedException {
        super.setupApp(PACKAGE_NAME,"wechat");
        mDevice.findObject(new UiSelector().text(Config.getProps("wechatChatName"))).click();
        messageBox = mDevice.findObject(new UiSelector().className("android.widget.EditText"));
        sendButton = mDevice.findObject(new UiSelector().text("Send"));
    }

    @Override
    void sendTextMessage(String message) throws UiObjectNotFoundException{
        messageBox.setText(message);
        sendButton.waitForExists(UI_DELAY);
        mDevice.click(SEND_COORD_X, SEND_COORD_Y);
    }

    @Override
    protected void sendImage() throws UiObjectNotFoundException {
        mDevice.findObject(new UiSelector().description("Hide more function buttons")).click();
        mDevice.findObject(new UiSelector().text("Album")).click();
        mDevice.findObject(new UiSelector().descriptionContains(Config.getProps("wechatImageName"))).click();
        mDevice.findObject(new UiSelector().text("Send")).click();
    }

}
