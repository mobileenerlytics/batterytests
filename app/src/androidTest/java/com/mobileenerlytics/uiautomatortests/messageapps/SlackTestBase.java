package com.mobileenerlytics.uiautomatortests.messageapps;

import android.support.test.filters.SdkSuppress;
import android.support.test.runner.AndroidJUnit4;
import android.support.test.uiautomator.UiObjectNotFoundException;
import android.support.test.uiautomator.UiSelector;

import org.junit.Before;
import org.junit.runner.RunWith;


/**
 * Created by srainha on 16/04/18.
 *
 * USAGE:
 *      - Test written for Nexus 6 (1440x2560 pixels)
 *      - Assumes EagleTester installed and setup
 *      - Assumes app installed
 *      - Assumes logged in and a chat exists
 *      - Note: This test will run inside of whatever convseration the app is in.
 */


@RunWith(AndroidJUnit4.class)
@SdkSuppress(minSdkVersion = 18)
public class SlackTestBase extends BaseMessageApps {

    private static final String PACKAGE_NAME = "com.Slack";

    @Before
    @Override
    public void setupChat() throws UiObjectNotFoundException, InterruptedException {
        super.setupApp(PACKAGE_NAME, "slack");
        messageBox = mDevice.findObject(new UiSelector().resourceId("com.Slack:id/message_input_field"));
        sendButton = mDevice.findObject(new UiSelector().description("Send message"));
    }

    @Override
    protected void sendImage() throws UiObjectNotFoundException {
        mDevice.findObject(new UiSelector().description("Open photos")).click();
        mDevice.findObject(new UiSelector().resourceId("com.Slack:id/image")).click();
        mDevice.findObject(new UiSelector().description("Send photo")).click();
    }
}
