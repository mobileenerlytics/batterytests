package com.mobileenerlytics.uiautomatortests.messageapps;

import android.content.Context;
import android.content.Intent;
import android.support.test.InstrumentationRegistry;
import android.support.test.filters.SdkSuppress;
import android.support.test.runner.AndroidJUnit4;
import android.support.test.uiautomator.By;
import android.support.test.uiautomator.UiDevice;
import android.support.test.uiautomator.UiObject;
import android.support.test.uiautomator.UiObjectNotFoundException;
import android.support.test.uiautomator.Until;

import com.mobileenerlytics.eagle.tester.logger.EagleTester;
import com.mobileenerlytics.uiautomatortests.Config;

import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.matcher.ViewMatchers.assertThat;
import static org.hamcrest.core.IsNull.notNullValue;

/**
 * Created by srainha on 16/04/18.
 *
 * USAGE:
 *      - Assumes EagleTester installed and setup
 *      - Assumes app installed
 *      - Assumes logged in and a chat exists
 *
 * EXTENDING THIS CLASS:
 *      - See other tests in this package for examples
 *      - Implement sendImage (every app handles this slightly differently)
 *      - Implement setupChat (often as simple as calling super.setupApp(PACKAGE_NAME) and
 *                              identifying the chatbox (EditText) and send button (Button))
 */

@RunWith(AndroidJUnit4.class)
@SdkSuppress(minSdkVersion = 18)
public abstract class BaseMessageApps {

    UiDevice mDevice;
    private static EagleTester eagleTester;
    private static final int LAUNCH_TIMEOUT = 5000;
    static final int UI_DELAY = 100;

    UiObject messageBox;
    UiObject sendButton;

    public void setupApp(String packageName, String appName) throws InterruptedException {
        eagleTester = new EagleTester(InstrumentationRegistry.getTargetContext());
        mDevice = UiDevice.getInstance(InstrumentationRegistry.getInstrumentation());
        mDevice.pressHome();
        final String launcherPackage = mDevice.getLauncherPackageName();
        assertThat(launcherPackage, notNullValue());
        mDevice.wait(Until.hasObject(By.pkg(launcherPackage).depth(0)), LAUNCH_TIMEOUT);
        Context context = InstrumentationRegistry.getContext();
        final Intent intent = context.getPackageManager().getLaunchIntentForPackage(packageName);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        context.startActivity(intent);
        mDevice.wait(Until.hasObject(By.pkg(packageName).depth(0)), LAUNCH_TIMEOUT);
        Thread.sleep(LAUNCH_TIMEOUT);
        assignProps(appName);
    }

    private static void assignProps(String appName){
        Config.setProps();
    }

    void sendTextMessage(String message) throws UiObjectNotFoundException {
        messageBox.setText(message);
        sendButton.click();
    }

    protected abstract void sendImage() throws UiObjectNotFoundException, InterruptedException;

    @Test
    public void batteryTest() throws InterruptedException, UiObjectNotFoundException {
        eagleTester.startMeasure("messageBatteryTest");
        sendTextMessage("Text 1");
        sendTextMessage("Text 2");
        sendTextMessage("Text 3");
        sendTextMessage("Text 4");
        sendTextMessage("Text 5");
        sendImage();
        sendTextMessage("Here is our website: http://mobileenerlytics.com/");
        sendTextMessage("Here is a video URL: https://www.youtube.com/watch?v=hNyMD8nFnoc");
        sendTextMessage("Here are some emoticons: ;) :D <3");
        sendTextMessage("Text 10 (last)");
        eagleTester.stopMeasure("messageBatteryTest");
    }

    @After
    public void tearDown() throws InterruptedException {
        eagleTester.finish();
    }

    public abstract void setupChat() throws UiObjectNotFoundException, InterruptedException;

}
