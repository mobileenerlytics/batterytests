package com.mobileenerlytics.uiautomatortests.messageapps;

import android.content.Context;
import android.content.Intent;
import android.support.test.InstrumentationRegistry;
import android.support.test.filters.SdkSuppress;
import android.support.test.runner.AndroidJUnit4;
import android.support.test.uiautomator.By;
import android.support.test.uiautomator.UiDevice;
import android.support.test.uiautomator.UiObject;
import android.support.test.uiautomator.UiObjectNotFoundException;
import android.support.test.uiautomator.UiSelector;
import android.support.test.uiautomator.Until;
import android.util.Log;

import com.mobileenerlytics.eagle.tester.logger.EagleTester;

import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.sql.Time;

import static android.support.test.espresso.matcher.ViewMatchers.assertThat;
import static org.hamcrest.core.IsNull.notNullValue;

@RunWith(AndroidJUnit4.class)
@SdkSuppress(minSdkVersion = 18)
public class LongTest {

    static UiDevice mDevice;
    private static EagleTester eagleTester;
    private static final int LAUNCH_TIMEOUT = 5000;
    private static final int UI_DELAY = 100;
    private static String PACKAGE_NAME = "com.whatsapp";
    private static final int TOTAL_DURATION = 120; // 2 minutes
    private static final int INTERVAL_DURATION = 60; // 1 minute intervals
    private static final String TAG = "LONG_TEST";

    static UiObject messageBox;
    static UiObject sendButton;

    @BeforeClass
    public static void setupApp() throws InterruptedException {
        eagleTester = new EagleTester(InstrumentationRegistry.getTargetContext());
        mDevice = UiDevice.getInstance(InstrumentationRegistry.getInstrumentation());
        mDevice.pressHome();
        final String launcherPackage = mDevice.getLauncherPackageName();
        assertThat(launcherPackage, notNullValue());
        mDevice.wait(Until.hasObject(By.pkg(launcherPackage).depth(0)), LAUNCH_TIMEOUT);
        Context context = InstrumentationRegistry.getContext();
        final Intent intent = context.getPackageManager().getLaunchIntentForPackage(PACKAGE_NAME);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        context.startActivity(intent);
        mDevice.wait(Until.hasObject(By.pkg(PACKAGE_NAME).depth(0)), LAUNCH_TIMEOUT);
        Thread.sleep(LAUNCH_TIMEOUT);
    }

    @Before
    public void setupChat() throws UiObjectNotFoundException, InterruptedException {
        mDevice.findObject(new UiSelector().text("Chat")).click();
        messageBox = mDevice.findObject(new UiSelector().text("Type a message"));
        sendButton = mDevice.findObject(new UiSelector().description("Send"));
    }

    @Test
    public void longTest() throws InterruptedException, UiObjectNotFoundException {
        int interval = 0;
        int totalTime = 0;
        Log.e("LONG-TEST", String.valueOf(TOTAL_DURATION));
        eagleTester.startMeasure("longTest_2_Hours");
        sendTextMessage("Starting long test: " + String.valueOf(TOTAL_DURATION) + "s");
        while (totalTime < TOTAL_DURATION) {
            sendTextMessage("Interval: " + String.valueOf(interval));
            sendTextMessage("Elapsed time: " + String.valueOf(totalTime) + "s");
            Log.e("Interval", String.valueOf(interval));
            Log.e("Elapsed time", String.valueOf(totalTime));
            interval++;
            totalTime += INTERVAL_DURATION;
            Thread.sleep(INTERVAL_DURATION * 1000);
        }
        eagleTester.stopMeasure("longTest_2_Hours");
    }

    private void sendTextMessage(String message) throws UiObjectNotFoundException {
        messageBox.setText(message);
        sendButton.waitForExists(UI_DELAY);
        sendButton.click();
    }

    @After
    public void tearDown() throws InterruptedException {
        eagleTester.finish();
    }

}
