package com.mobileenerlytics.uiautomatortests.messageapps;

import android.support.test.filters.SdkSuppress;
import android.support.test.runner.AndroidJUnit4;
import android.support.test.uiautomator.By;
import android.support.test.uiautomator.UiObjectNotFoundException;
import android.support.test.uiautomator.UiSelector;

import org.junit.Before;
import org.junit.runner.RunWith;

/**
 * Created by srainha on 16/04/18.
 *
 * USAGE:
 *      - Test written for Nexus 6 (1440x2560 pixels)
 *      - Assumes EagleTester installed and setup
 *      - Assumes app installed
 *      - Assumes logged in and a chat exists
 *      - Note: This test selects the top (most recent) conversation
 *      - Note: Will select the most recent image in the Download folder
 */

@RunWith(AndroidJUnit4.class)
@SdkSuppress(minSdkVersion = 18)
public class HikeTestBase extends BaseMessageApps {

    @Before
    @Override
    public void setupChat() throws UiObjectNotFoundException, InterruptedException {
        super.setupApp("com.bsb.hike","hike");
        mDevice.findObject(new UiSelector().resourceId("com.bsb.hike:id/conversation_item")).click();
        messageBox = mDevice.findObject(new UiSelector().resourceId("com.bsb.hike:id/msg_compose"));
        sendButton = mDevice.findObject(new UiSelector().resourceId("com.bsb.hike:id/attachment_view"));
    }

    @Override
    protected void sendImage() throws UiObjectNotFoundException {
        mDevice.findObject(new UiSelector().resourceId("com.bsb.hike:id/attach_wa_inside_btn")).click();
        mDevice.findObjects(By.res("com.bsb.hike:id/x_card_icon")).get(2).click();
        mDevice.findObject(new UiSelector().resourceId("com.bsb.hike:id/folderimage")).click();
        mDevice.findObject(new UiSelector().resourceId("com.bsb.hike:id/album_image")).click();
        mDevice.findObject(new UiSelector().resourceId("com.bsb.hike:id/album_image")).click();
        mDevice.findObject(new UiSelector().resourceId("com.bsb.hike:id/send")).click();
        mDevice.findObject(new UiSelector().text("SEND")).click();
    }

}
