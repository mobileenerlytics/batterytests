package com.mobileenerlytics.uiautomatortests.notes;


import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;
import android.support.test.uiautomator.By;
import android.support.test.uiautomator.UiDevice;
import android.support.test.uiautomator.UiObjectNotFoundException;
import android.support.test.uiautomator.Until;

import com.mobileenerlytics.eagle.tester.logger.EagleTester;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.assertThat;

@RunWith(AndroidJUnit4.class)
public abstract class BaseNotes {
    protected UiDevice mDevice;
    private static final int LAUNCH_TIMEOUT = 5000;
    private static StringBuilder resourceId;
    protected static EagleTester eagleTester;

    @BeforeClass
    public static void setUp() throws InterruptedException {
        eagleTester = new EagleTester(InstrumentationRegistry.getTargetContext());
    }

    public void setUpApp(String PACKAGE_NAME) throws InterruptedException {
        // Initialize UiDevice instance
        mDevice = UiDevice.getInstance(InstrumentationRegistry.getInstrumentation());
        assertThat(mDevice, notNullValue());
        mDevice.pressHome();

        final String launcherPackage = getLauncherPackageName();
        assertThat(launcherPackage, notNullValue());
        mDevice.wait(Until.hasObject(By.pkg(launcherPackage).depth(0)), LAUNCH_TIMEOUT);

        // Launch the blueprint app
        Context context = InstrumentationRegistry.getContext();
        final Intent intent = context.getPackageManager()
                .getLaunchIntentForPackage(PACKAGE_NAME);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);    // Clear out any previous instances
        context.startActivity(intent);

        // Wait for the app to appear
        mDevice.wait(Until.hasObject(By.pkg(PACKAGE_NAME).depth(0)), LAUNCH_TIMEOUT);
    }

    private String getLauncherPackageName() {
        // Create launcher Intent
        final Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_HOME);

        // Use PackageManager to get the launcher package name
        PackageManager pm = InstrumentationRegistry.getContext().getPackageManager();
        ResolveInfo resolveInfo = pm.resolveActivity(intent, PackageManager.MATCH_DEFAULT_ONLY);
        return resolveInfo.activityInfo.packageName;
    }

    protected abstract void createNote() throws InterruptedException, UiObjectNotFoundException;
    protected abstract void createNoteWithPen() throws InterruptedException, UiObjectNotFoundException;

    @Test
    public void createNoteTest() throws Exception {
        eagleTester.startMeasure("createNoteTest");
        Thread.sleep(1000);
        createNote();
        eagleTester.stopMeasure("createNoteTest");
    }

    @Test
    public void createNoteWithPenTest() throws Exception {
        eagleTester.startMeasure("createNoteWithPenTest");
        Thread.sleep(1000);
        createNoteWithPen();
        eagleTester.stopMeasure("createNoteWithPenTest");
    }

    @AfterClass
    public static void destroyEagleTester() throws InterruptedException {
        eagleTester.finish();
    }
}

