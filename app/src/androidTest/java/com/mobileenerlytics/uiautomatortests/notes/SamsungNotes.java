package com.mobileenerlytics.uiautomatortests.notes;

import android.support.test.uiautomator.UiObject;
import android.support.test.uiautomator.UiObjectNotFoundException;
import android.support.test.uiautomator.UiSelector;

import org.junit.Before;

public class SamsungNotes extends BaseNotes {
    private static final String PACKAGE_NAME = "com.samsung.android.app.notes";
    private static final String TITLE_TEXT = "TESTER";
    private static final String NOTE_TEXT = "This is for testing energy drain";

    @Before
    public void before() throws InterruptedException {
        super.setUpApp(PACKAGE_NAME);
    }

    @Override
    protected void createNote() throws InterruptedException, UiObjectNotFoundException {
        UiObject addNote = mDevice.findObject(new UiSelector().resourceId("com.samsung.android.app.notes:id/fab"));
        addNote.click();
        UiObject setTitle = mDevice.findObject(new UiSelector().resourceId("com.samsung.android.app.notes:id/editor_title"));
        setTitle.setText(TITLE_TEXT);
        UiObject editText = mDevice.findObject(new UiSelector().resourceId("com.samsung.android.app.notes:id/content_text"));
        editText.setText(NOTE_TEXT);
        UiObject optionsIcon = mDevice.findObject(new UiSelector().className("android.widget.ImageView").index(1));
        optionsIcon.click();
        UiObject save = mDevice.findObject(new UiSelector().text("Save"));
        save.click();
    }

    @Override
    protected void createNoteWithPen() throws InterruptedException, UiObjectNotFoundException {
        UiObject addNote = mDevice.findObject(new UiSelector().resourceId("com.samsung.android.app.notes:id/fab"));
        addNote.click();
        UiObject selectPen = mDevice.findObject(new UiSelector().resourceId("com.samsung.android.app.notes:id/writingBtn"));
        selectPen.click();
        UiObject setTitle = mDevice.findObject(new UiSelector().resourceId("com.samsung.android.app.notes:id/editor_title"));
        setTitle.setText(TITLE_TEXT);
        UiObject drawText = mDevice.findObject(new UiSelector().className("android.view.View").index(0));
        drawText.dragTo(10,50,5);
        drawText.dragTo(0,1000,5);
        UiObject optionsIcon = mDevice.findObject(new UiSelector().className("android.widget.ImageView").index(1));
        optionsIcon.click();
        UiObject save = mDevice.findObject(new UiSelector().text("Save"));
        save.click();
    }
}
