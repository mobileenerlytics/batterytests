package com.mobileenerlytics.uiautomatortests.browser;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.support.test.InstrumentationRegistry;
import android.support.test.uiautomator.By;
import android.support.test.uiautomator.UiDevice;
import android.support.test.uiautomator.UiObjectNotFoundException;
import android.support.test.uiautomator.Until;

import com.mobileenerlytics.eagle.tester.logger.EagleTester;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.assertThat;

public abstract class BaseBrowser {

    protected UiDevice mDevice;
    private static final int LAUNCH_TIMEOUT = 5000;

    protected static EagleTester eagleTester;

    @BeforeClass
    public static void setUp() throws InterruptedException {
        eagleTester = new EagleTester(InstrumentationRegistry.getTargetContext());
    }

    public void setUpApp(String PACKAGE_NAME) throws InterruptedException {
        eagleTester = new EagleTester(InstrumentationRegistry.getTargetContext());
        // Initialize UiDevice instance
        mDevice = UiDevice.getInstance(InstrumentationRegistry.getInstrumentation());
        assertThat(mDevice, notNullValue());
        mDevice.pressHome();

        final String launcherPackage = getLauncherPackageName();
        assertThat(launcherPackage, notNullValue());
        mDevice.wait(Until.hasObject(By.pkg(launcherPackage).depth(0)), LAUNCH_TIMEOUT);

        // Launch the blueprint app
        Context context = InstrumentationRegistry.getContext();
        final Intent intent = context.getPackageManager()
                .getLaunchIntentForPackage(PACKAGE_NAME);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);    // Clear out any previous instances
        context.startActivity(intent);

        // Wait for the app to appear
        mDevice.wait(Until.hasObject(By.pkg(PACKAGE_NAME).depth(0)), LAUNCH_TIMEOUT);

    }

    private String getLauncherPackageName() {
        // Create launcher Intent
        final Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_HOME);

        // Use PackageManager to get the launcher package name
        PackageManager pm = InstrumentationRegistry.getContext().getPackageManager();
        ResolveInfo resolveInfo = pm.resolveActivity(intent, PackageManager.MATCH_DEFAULT_ONLY);
        return resolveInfo.activityInfo.packageName;
    }

    protected abstract void searchLinks() throws UiObjectNotFoundException;
    protected abstract void clickOnMore() throws InterruptedException, UiObjectNotFoundException;
    protected abstract void bookmarks() throws UiObjectNotFoundException;
    protected abstract void openTabs() throws UiObjectNotFoundException, InterruptedException;

    @Test
    protected void searchLinksTest() throws UiObjectNotFoundException, InterruptedException {
        eagleTester.startMeasure("searchLinksTest");
        searchLinks();
        eagleTester.stopMeasure("searchLinksTest");
    }

    @Test
    protected void clickOnMoreTest() throws UiObjectNotFoundException, InterruptedException {
        eagleTester.startMeasure("clickOnMoreTest");
        clickOnMore();
        eagleTester.stopMeasure("clickOnMoreTest");
    }

    @Test
    protected void bookmarksTest() throws UiObjectNotFoundException, InterruptedException {
        eagleTester.startMeasure("bookmarksTest");
        bookmarks();
        eagleTester.stopMeasure("bookmarksTest");
    }

    @Test
    protected void openTabsTest() throws UiObjectNotFoundException, InterruptedException {
        eagleTester.startMeasure("openTabsTest");
        openTabs();
        eagleTester.stopMeasure("openTabsTest");
    }

    @AfterClass
    public void tearDown() throws InterruptedException {
        eagleTester.finish();
    }

}
