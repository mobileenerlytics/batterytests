package com.mobileenerlytics.uiautomatortests.browser;

import android.support.test.uiautomator.UiObject;
import android.support.test.uiautomator.UiObjectNotFoundException;
import android.support.test.uiautomator.UiScrollable;
import android.support.test.uiautomator.UiSelector;

import org.junit.Before;

public class SamsungBrowser extends BaseBrowser{
    private static final String PACKAGE_NAME = "com.sec.android.app.sbrowser";
    private static final String SEARCH_LINK = "google.com";

    @Before
    public void before() throws InterruptedException {
        super.setUpApp(PACKAGE_NAME);
    }

    @Override
    protected void searchLinks() throws UiObjectNotFoundException {
        UiObject urlBar = mDevice.findObject(new UiSelector().resourceId("com.sec.android.app.sbrowser:id/sbrowser_url_bar"));
        urlBar.click();
        urlBar.setText(SEARCH_LINK);
        mDevice.pressEnter();
        UiObject allowLocation = mDevice.findObject(new UiSelector().text("Allow"));
        if(allowLocation.exists()){
            allowLocation.click();
        }
    }

    @Override
    protected void clickOnMore() throws InterruptedException, UiObjectNotFoundException {
        UiObject more = mDevice.findObject(new UiSelector().resourceId("com.sec.android.app.sbrowser:id/ph_toolbar_option_menu"));
        Thread.sleep(10000);
        more.click();
        UiSelector frameLayout = new UiSelector().className("android.widget.FrameLayout");
        UiScrollable scroll = new UiScrollable(frameLayout);
        scroll.setAsVerticalList();
        scroll.scrollToEnd(5);
        UiObject settings = mDevice.findObject(new UiSelector().className("android.widget.LinearLayout").index(6));
        settings.click();
        UiObject back = mDevice.findObject(new UiSelector().descriptionContains("Navigate up").className("android.widget.ImageButton"));
        back.click();
    }

    @Override
    protected void bookmarks() throws UiObjectNotFoundException {
        UiObject bookmark = mDevice.findObject(new UiSelector().resourceId("com.sec.android.app.sbrowser:id/ph_toolbar_bookmarks"));
        bookmark.click();
        UiObject addBookmark = mDevice.findObject(new UiSelector().resourceId("com.sec.android.app.sbrowser:id/action_button_add_bookmark"));
        addBookmark.click();
        UiObject bookmarkTitle = mDevice.findObject(new UiSelector().resourceId("com.sec.android.app.sbrowser:id/add_bookmark_page_title_input"));
        bookmarkTitle.setText("Google");
        UiObject bookmarkLink = mDevice.findObject(new UiSelector().resourceId("com.sec.android.app.sbrowser:id/add_bookmark_page_url_input"));
        bookmarkLink.setText(SEARCH_LINK);
        UiObject saveBookmark = mDevice.findObject(new UiSelector().resourceId("com.sec.android.app.sbrowser:id/actionbar_save_button"));
        saveBookmark.click();
        UiObject back = mDevice.findObject(new UiSelector().descriptionContains("Navigate up").className("android.widget.ImageButton"));

    }

    @Override
    protected void openTabs() throws UiObjectNotFoundException, InterruptedException {
        UiObject tabs = mDevice.findObject(new UiSelector().resourceId("com.sec.android.app.sbrowser:id/ph_toolbar_multiwindow_count"));
        tabs.click();
        Thread.sleep(1000);
        mDevice.click(1140,2436);
        Thread.sleep(1000);
    }
}
