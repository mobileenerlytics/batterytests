# Mobile Enerlytics Eagle-Tester battery tests     
  
**Background**  
This repository holds a variety of UiAutomator tests designed using MobileEnerlytics' battery profiling software **EagleTester**. With 2 lines of code one can quickly run a battery profiling test, making energy profiling integration into existing tests easy and fast. Below is a tutorial for setting up tests on a messaging app, but the concepts apply to any app.  
  


### Tutorial: Installing and Running a Messaging App Test

**Summary**  

This guide will walk through the steps necessary to install and setup a messaging app and test it with Mobile Enerlytics' EagleTester app. This example will use **WeChat** but the general steps should be applicable to many or all messaging apps on the Play Store.
Note: The test sends 9 text messages and the most recent downloaded image. Let's say the image on the phone is called IMAGE.JPG


**Setting up the Messaging App**

* Download app from Play Store
* Create account (or just log in if you already have one)
* Create a new chat or group chat to send messages (E.g CHAT_NAME)
* Optional: Send a message to check that it works

**Installing Tests**

* Clone battery tests
    * git clone git@bitbucket.org:mobileenerlytics/batterytests.git
* Create properties file
    * Path:  *app/src/androidTest/assets/config.properties*
    * wechatChatName=CHAT_NAME
    * wechatImageName=IMAGE_NAME
* Install tests on Android device
    * ./gradlew installDebug && ./gradlew installDebugAndroidTest
    * Or, install it with Android Studio

**Running Tests with Jenkins**

* Go to the Jenkins project, then click "Configure" > "Eagle Tester"
    * Package name: *com.tencent.mm*
    * Commit: $BUILD_NUMBER
    * Author, email: can be anything
* Click "Add Build Step" > "Execute Shell Command"
    * Enter command below
    * Note: You can find this command and others by running the test from Android Studio



*adb shell am instrument -w -r   -e debug false -e class com.mobileenerlytics.uiautomatortests.messageapps.WeChatTestBase com.mobileenerlytics.uiautomatortests.test/android.support.test.runner.AndroidJUnitRunner*


